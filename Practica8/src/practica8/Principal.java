package practica8;

import java.sql.SQLException;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub

		ConectandoConDatos misDatos = new ConectandoConDatos();
		Scanner input = new Scanner(System.in);
		misDatos.conectar();
		int opcion = 0;
		int tipo = 0;
		String nombre = "";
		float precio = 0;
		do {
			do {
				System.out.println("MENU");
				System.out.println("1- Insertar mueble");
				System.out.println("2- Actualizar mueble");
				System.out.println("3- Eliminar mueble");
				System.out.println("4- Mostrar base de datos");
				System.out.println("5- Desconectar de la base de datos");

				System.out.print("Introduce opcion: ");
				opcion = input.nextInt();
				input.nextLine();
			} while (opcion < 1 || opcion > 5);

			switch (opcion) {

			case 1:
				String respuesta = "";
				do {
					System.out.println("Insertamos datos");
					System.out.println(
							"Tipo de mueble('1- Armario', '2- Aparador', '3- Cabecedor', '4- Comoda', '5- Consola')");
					System.out.print("Introduce numero: ");
					tipo = input.nextInt();
					input.nextLine();
					System.out.print("Nombre del mueble: ");
					nombre = input.nextLine();
					System.out.print("Precio del mueble: ");
					precio = input.nextFloat();
					misDatos.insertar(tipo, nombre, precio);
					input.nextLine();
					System.out.print("�Deseas continuar?(Si o No): ");
					respuesta = input.nextLine();
				} while (respuesta.equalsIgnoreCase("Si"));
				break;

			case 2:
				System.out.println("Actualizamos datos");
				System.out.print("Nombre del mueble a modificar: ");
				nombre = input.nextLine();
				System.out.println(
						"Tipo de mueble('1- Armario', '2- Aparador', '3- Cabecedor', '4- Comoda', '5- Consola')");
				System.out.print("Introduce numero: ");
				tipo = input.nextInt();
				System.out.print("Precio del mueble: ");
				precio = input.nextFloat();
				misDatos.actualizar(nombre, tipo, precio);
				input.nextLine();
				break;

			case 3:
				String eliminar = "";
				do {
					System.out.println("Eliminamos datos");
					System.out.print("Dame el nombre del mueble: ");
					nombre = input.nextLine();
					misDatos.eliminar(nombre);
					System.out.print("�Deseas continuar?(Si o No): ");
					eliminar = input.nextLine();
				} while (eliminar.equalsIgnoreCase("Si"));
				break;

			case 4:

				System.out.println("MOSTRAMOS DATOS");
				misDatos.seleccionar();
				break;
			case 5:
				System.out.println("Desconectamos");
				misDatos.desconectar();

				break;

			}
		} while (opcion != 5);

	}

}

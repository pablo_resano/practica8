package practica8;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConectandoConDatos {

	private Connection conexion=null;
	PreparedStatement sentencia=null;
	
	//Conectar con base de datos
	public void conectar() throws SQLException {
		String servidor = "jdbc:mysql://localhost:3306/";
		String bbdd="muebles";
		String user="root";
		String password="";
		conexion=DriverManager.getConnection(servidor+bbdd,user,password);	
	}
	
	
	
	public void seleccionar() throws SQLException {
		String sentenciaSql="SELECT * FROM muebles";
		sentencia=conexion.prepareStatement(sentenciaSql);
		ResultSet resultado=sentencia.executeQuery();
		//mostramos los datos
		while (resultado.next()) {
			System.out.println(resultado.getInt(1)+ ", "+ resultado.getString(2)+", "+
					resultado.getString(3)+ ", "+
					resultado.getFloat(4));
		}		
	}
	
	
	public void insertar (int tipo, String nombre, float precio) throws SQLException {
		String sentenciaSql="INSERT INTO muebles(tipo, nombre, precio) "+
				"values(?,?,?)";
		PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
		sentencia.setInt(1,tipo);
		sentencia.setString(2, nombre);
		sentencia.setFloat(3, precio);
		sentencia.executeUpdate();
	}
	
	
	public void actualizar(String nombre, int tipo, float precio) throws SQLException {
		String sentenciaSql="UPDATE muebles set "+
				"tipo=?, precio=? WHERE nombre=?";
		PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
		sentencia.setInt(1,tipo);
		sentencia.setFloat(2, precio);
		sentencia.setString(3, nombre);
		sentencia.executeUpdate();
	}
	
	
	public void eliminar(String nombre) throws SQLException {
		String sentenciaSql="DELETE FROM muebles WHERE nombre=?";
		PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
		sentencia.setString(1,nombre);
		sentencia.executeUpdate();
	}
	
	public void desconectar() throws SQLException {
		sentencia.close();
	}
	

}

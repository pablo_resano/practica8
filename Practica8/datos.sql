
CREATE DATABASE muebles;

CREATE TABLE muebles(Id INT(11)AUTO_INCREMENT, tipo ENUM ('Armario', 'Aparador','Cabecedor','Comoda','Consola') DEFAULT NULL, nombre VARCHAR(50) DEFAULT NULL, precio FLOAT DEFAULT NULL, PRIMARY KEY (Id));

INSERT INTO muebles VALUES (1, "Armario", "Mueble1", 30);
INSERT INTO muebles VALUES (2, "Aparador", "Mueble2", 10);
INSERT INTO muebles VALUES (3, "Cabecedor", "Mueble3", 40);
INSERT INTO muebles VALUES (4, "Comoda", "Mueble4", 100);
INSERT INTO muebles VALUES (5, "Consola", "Mueble5", 88);
INSERT INTO muebles VALUES (6, "Armario", "Mueble6", 30);
INSERT INTO muebles VALUES (7, "Aparador", "Mueble7", 10);
INSERT INTO muebles VALUES (8, "Cabecedor", "Mueble8", 40);
INSERT INTO muebles VALUES (9, "Comoda", "Mueble9", 100);
INSERT INTO muebles VALUES (10, "Consola", "Mueble10", 88);



